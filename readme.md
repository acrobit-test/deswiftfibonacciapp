# DESwiftFibonacciApp

Implementation of an app that will make use of the Fibonacci calculation framwork. 

The application works for iOS 15 and iOS 16.

The project imports the framework using SPM, through the git url:
https://gitlab.com/acrobit-test/deswiftfibonaccipackage

The application uses a basic MVVM pattern for presenting and building the UI. For navigation (although it only consists of 2 screens) the Coordinator pattern is used.

## Testing

Additionally a couple of UI Tests have been implemented which will do the following:

1. Start the app
2. Tap the start button in the initial screen
3. Enter a number in the textfield
4. Tap the submit button, which will start the calculation
5. Wait until an alert with the result of the calculation is presented
6. Validate that the alert contains the expected result