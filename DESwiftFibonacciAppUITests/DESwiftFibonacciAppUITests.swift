//
//  DESwiftFibonacciAppUITests.swift
//  DESwiftFibonacciAppUITests
//
//  Created by Diego Espinoza on 31/01/23.
//

import XCTest

final class DESwiftFibonacciAppUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFibonacciInput9() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let startButton = app.buttons["Start button"]
        startButton.tap()
        
        let textfield = app.textFields["Input textfield"]
        textfield.typeText("9")
        
        let submitButton = app.buttons["Submit button"]
        submitButton.tap()
  
        let exists = NSPredicate(format: "exists == true")
        let alert = app.alerts.firstMatch
        expectation(for: exists, evaluatedWith: alert)
        
        waitForExpectations(timeout: 10)
        
        let labelPredicate = NSPredicate(format: "label CONTAINS[cd] '34'")
        let label = alert.staticTexts.containing(labelPredicate).firstMatch
        
        XCTAssert(label.exists)
    }
    
    func testFibonacciInput10() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let startButton = app.buttons["Start button"]
        startButton.tap()
        
        let textfield = app.textFields["Input textfield"]
        textfield.typeText("10")
        
        let submitButton = app.buttons["Submit button"]
        submitButton.tap()
  
        let exists = NSPredicate(format: "exists == true")
        let alert = app.alerts.firstMatch
        expectation(for: exists, evaluatedWith: alert)
        
        waitForExpectations(timeout: 10)
        
        let labelPredicate = NSPredicate(format: "label CONTAINS[cd] '55'")
        let label = alert.staticTexts.containing(labelPredicate).firstMatch
        
        XCTAssert(label.exists)
    }
    
    func testFibonacciInput0() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let startButton = app.buttons["Start button"]
        startButton.tap()
        
        let textfield = app.textFields["Input textfield"]
        textfield.typeText("0")
        
        let submitButton = app.buttons["Submit button"]
        submitButton.tap()
  
        let exists = NSPredicate(format: "exists == true")
        let alert = app.alerts.firstMatch
        expectation(for: exists, evaluatedWith: alert)
        
        waitForExpectations(timeout: 10)
        
        let labelPredicate = NSPredicate(format: "label CONTAINS[cd] '0'")
        let label = alert.staticTexts.containing(labelPredicate).firstMatch
        
        XCTAssert(label.exists)
    }
    
    func testSubmitIsDisabled() throws {
        let app = XCUIApplication()
        app.launch()

        let startButton = app.buttons["Start button"]
        startButton.tap()
        
        let textfield = app.textFields["Input textfield"]
        textfield.typeText("100")
        
        let submitButton = app.buttons["Submit button"]
        XCTAssert(submitButton.isEnabled == false)
    }
    
    func testSubmitIsEnabled() throws {
        let app = XCUIApplication()
        app.launch()

        let startButton = app.buttons["Start button"]
        startButton.tap()
        
        let textfield = app.textFields["Input textfield"]
        textfield.typeText("50")
        
        let submitButton = app.buttons["Submit button"]
        XCTAssert(submitButton.isEnabled)
    }
}
