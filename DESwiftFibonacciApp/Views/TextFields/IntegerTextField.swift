//
//  IntegerTextField.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit
import Combine

open class IntegerTextField: UITextField {
    var onTextChange: AnyPublisher<String?, Never> { _onTextChange.eraseToAnyPublisher() }
    
    private var _onTextChange: PassthroughSubject<String?, Never> = .init()
    
    init(placeholder: String, accessibilityLabel: String) {
        super.init(frame: .zero)
        self.placeholder = placeholder
        self.accessibilityLabel = accessibilityLabel
        setupView()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        delegate = self
        borderStyle = .roundedRect
        font = UIFont.preferredFont(forTextStyle: .body, weight: .regular)
        heightAnchor.constraint(equalToConstant: .xxLarge).isActive = true
        keyboardType = .numberPad
        returnKeyType = .done
        addTarget(self, action: #selector(onTextChange(sender:)), for: .editingChanged)
    }

    @objc private func onTextChange(sender: IntegerTextField) {
        _onTextChange.send(sender.text)
    }
}

extension IntegerTextField: UITextFieldDelegate {
    public func textField(_ textField: UITextField,
                          shouldChangeCharactersIn range: NSRange,
                          replacementString string: String) -> Bool {
        let characterSet = CharacterSet(charactersIn: string)
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: characterSet) else {
            return false
        }
        return true
    }
}
