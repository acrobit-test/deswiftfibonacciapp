//
//  +UILabel.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit

extension UILabel {
    static func makeLargeTitle(text: String, accessibilityLabel: String) -> UILabel {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .largeTitle, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.numberOfLines = 0
        label.textColor = .label
        label.accessibilityLabel = accessibilityLabel
        return label
    }
    
    static func makeSubtitle(text: String, accessibilityLabel: String) -> UILabel {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.numberOfLines = 0
        label.textColor = .secondaryLabel
        label.accessibilityLabel = accessibilityLabel
        return label
    }
    
    static func makeTitle(text: String, accessibilityLabel: String) -> UILabel {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.numberOfLines = 2
        label.textColor = .label
        label.accessibilityLabel = accessibilityLabel
        return label
    }
    
    static func makeFooter(text: String, accessibilityLabel: String) -> UILabel {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .subheadline, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.numberOfLines = 0
        label.textColor = .secondaryLabel
        label.accessibilityLabel = accessibilityLabel
        return label
    }
}
