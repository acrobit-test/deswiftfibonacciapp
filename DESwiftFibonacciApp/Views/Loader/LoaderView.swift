//
//  LoaderView.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit

final class LoaderView: UIView {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.startAnimating()
        return indicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        backgroundColor = .tertiarySystemBackground.withAlphaComponent(0.95)
        layer.cornerRadius = .small
        addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            activityIndicator.topAnchor.constraint(equalTo: topAnchor, constant: .large),
            activityIndicator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .large),
            activityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.large),
            activityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.large)
        ])
    }
}
