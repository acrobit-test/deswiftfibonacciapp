//
//  LoaderPresentable.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit

protocol LoaderPresentable {
    var loaderView: UIView { get }
    func setupLoader()
    func presentLoader(_ isPresented: Bool)
}

extension LoaderPresentable where Self: UIView {
    func setupLoader() {
        addSubview(loaderView)
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            loaderView.centerYAnchor.constraint(equalTo: centerYAnchor),
            loaderView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        loaderView.alpha = 0
        loaderView.isHidden = true
        loaderView.bringSubviewToFront(self)
    }
    
    func presentLoader(_ isPresented: Bool) {
        if isPresented {
            loaderView.alpha = 0
            loaderView.isHidden = false
            loaderView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 0.9,
                           initialSpringVelocity: 0.9,
                           animations: {
                self.loaderView.alpha = 1
                self.loaderView.transform = .identity
            })
        } else {
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           usingSpringWithDamping: 0.9,
                           initialSpringVelocity: 0.9,
                           animations: {
                self.loaderView.alpha = 0
                self.loaderView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            },
                           completion: { _ in
                self.loaderView.isHidden = true
                self.loaderView.transform = .identity
            })
        }
    }
}

