//
//  CalculationInputView.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit
import Combine

final class CalculationInputView: UIView {
    var onTextChange: AnyPublisher<String?, Never> { textField.onTextChange }
    var isInputEnabled: Bool {
        get { textField.isEnabled }
        set { textField.isEnabled = newValue }
    }
    override var isFirstResponder: Bool {
        get { textField.isFirstResponder }
        set {
            if newValue {
                textField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        }
    }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = .medium
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var textField: IntegerTextField = {
        let textField = IntegerTextField(placeholder: "Enter a positive number",
                                         accessibilityLabel: "Input textfield")
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var titleLabel: UILabel = {
        return .makeTitle(text: "Enter number to calculate",
                          accessibilityLabel: "Number input")
    }()
    
    private lazy var subtitleLabel: UILabel = {
        return .makeFooter(text: "The max number allowed for the calculation will be \(Constants.maxNumber).",
                           accessibilityLabel: "footer label")
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(subtitleLabel)
        layer.cornerRadius = .xSmall
        backgroundColor = .secondarySystemGroupedBackground
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: .medium),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .medium),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.medium),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.large)
        ])
    }
}
