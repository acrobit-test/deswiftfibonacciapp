//
//  CalculationViewModel.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import Foundation
import DESwiftFibonacci
import Combine

protocol CalculationViewModelType: AnyObject {
    var isCalculating: AnyPublisher<Bool, Never> { get }
    var calculationResult: AnyPublisher<Int64, Never> { get }
    var inputValue: Int { get set }
    
    func calculateFibonacci()
    func cancelCalculation()
}

final class CalculationViewModel<C>: CalculationViewModelType where C: CalculationFlowCoordinator {
    var calculationResult: AnyPublisher<Int64, Never> { _calculationResult.eraseToAnyPublisher() }
    var isCalculating: AnyPublisher<Bool, Never> { _isCalculating.eraseToAnyPublisher() }
    var inputValue: Int = 0
    
    private weak var coordinator: C?
    private let fibonacci = DEFibonacci()
    
    private var _calculationResult: PassthroughSubject<Int64, Never> = .init()
    private var _isCalculating: PassthroughSubject<Bool, Never> = .init()
    
    init(coordinator: C) {
        self.coordinator = coordinator
    }
    
    func calculateFibonacci() {
        _isCalculating.send(true)
        fibonacci.calculate(usingNumber: inputValue) { [weak self] result in
            self?._calculationResult.send(result)
            self?._isCalculating.send(false)
        }
    }
    
    func cancelCalculation() {
        fibonacci.cancelCalculation()
        _isCalculating.send(false)
    }
}
