//
//  CalculationView.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit
import Combine

final class CalculationView: UIView, LoaderPresentable {
    var loaderView: UIView { _loaderView }
    
    private lazy var calculationInputView: CalculationInputView = {
        let view = CalculationInputView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = .medium
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var submitButton: UIButton = {
        return .makeFilled(title: "Submit",
                           accessibilityLabel: "Submit button")
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton.makeFilled(title: "Cancel",
                                         accessibilityLabel: "Cancel button")
        button.tintColor = .systemRed
        return button
    }()
    
    private let _loaderView = LoaderView()
    private let viewModel: CalculationViewModelType
    private let _showAlert: PassthroughSubject<String, Never> = .init()
    private var cancellables = Set<AnyCancellable>()
    
    init(viewModel: CalculationViewModelType) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
        setupLayout()
        setupBindings()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLoader() {
        addSubview(loaderView)
        loaderView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            loaderView.centerXAnchor.constraint(equalTo: calculationInputView.centerXAnchor),
            loaderView.centerYAnchor.constraint(equalTo: calculationInputView.centerYAnchor)
        ])

        loaderView.alpha = 0
        loaderView.isHidden = true
        loaderView.bringSubviewToFront(self)
    }
    
    private func setupBindings() {
        viewModel
            .isCalculating
            .sink { [weak self] isCalculating in
                self?.submitButton.isEnabled = !isCalculating
                self?.cancelButton.isEnabled = isCalculating
                self?.calculationInputView.isInputEnabled = !isCalculating
                self?.presentLoader(isCalculating)
            }
            .store(in: &cancellables)
        
        let onTextChange = calculationInputView
            .onTextChange
            .share()
        
        onTextChange
            .map { text -> Bool in
                guard let text = text else { return false }
                guard let value = Int(text) else { return false }
                return !text.isEmpty && value <= Constants.maxNumber
            }
            .sink { [weak self] isEnabled in
                self?.submitButton.isEnabled = isEnabled
            }
            .store(in: &cancellables)
        
        onTextChange
            .compactMap { text -> Int? in
                guard let text = text else { return nil }
                return Int(text)
            }
            .sink { [weak self] value in
                self?.viewModel.inputValue = value
            }
            .store(in: &cancellables)
    }
    
    private func setupView() {
        backgroundColor = .systemGroupedBackground
        addSubview(calculationInputView)
        addSubview(buttonStackView)
        buttonStackView.addArrangedSubview(submitButton)
        buttonStackView.addArrangedSubview(cancelButton)
        
        calculationInputView.isFirstResponder = true
        cancelButton.isEnabled = false
        submitButton.isEnabled = false
        cancelButton.addTarget(self, action: #selector(onCancelTap), for: .touchUpInside)
        submitButton.addTarget(self, action: #selector(onSubmitTap), for: .touchUpInside)
        setupLoader()
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            calculationInputView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor,
                                                      constant: .large),
            calculationInputView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor,
                                                          constant: .medium),
            calculationInputView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor,
                                                           constant: -.medium)
        ])
        
        NSLayoutConstraint.activate([
            buttonStackView.topAnchor.constraint(equalTo: calculationInputView.bottomAnchor,
                                                 constant: .xMedium),
            buttonStackView.leadingAnchor.constraint(equalTo: calculationInputView.leadingAnchor),
            buttonStackView.trailingAnchor.constraint(equalTo: calculationInputView.trailingAnchor)
        ])
    }
    
    
    @objc private func onSubmitTap() {
        viewModel.calculateFibonacci()
    }
    
    @objc private func onCancelTap() {
        viewModel.cancelCalculation()
    }
}
