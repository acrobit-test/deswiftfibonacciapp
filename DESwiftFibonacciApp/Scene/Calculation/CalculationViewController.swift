//
//  CalculationViewController.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit
import Combine

protocol CalculationFlowCoordinator: AnyObject {
}

final class CalculationViewController<VM>: UIViewController where VM: CalculationViewModelType {
    private let rootView: CalculationView
    private let viewModel: VM
    private var cancellables = Set<AnyCancellable>()
    
    init(rootView: CalculationView, viewModel: VM) {
        self.rootView = rootView
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        view = rootView
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Calculations"
        setupBindings()
    }
    
    private func setupBindings() {
        viewModel
            .calculationResult
            .sink { [weak self] result in
                guard let owner = self else { return }
                if result == -1 {
                    owner.showAlert(message: "The calculation was canceled")
                } else {
                    owner.showAlert(message: "The fibonacci for \(owner.viewModel.inputValue) is: \(result)")
                }
            }
            .store(in: &cancellables)
    }
    
    private func showAlert(message: String) {
        let alert = UIAlertController(title: "Calculation Result",
                                      message: message,
                                      preferredStyle: .alert)
        alert.accessibilityLabel = "Result alert"
        alert.accessibilityValue = message
        let closeAction = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(closeAction)
        present(alert, animated: true)
    }
}
