//
//  StartUpView.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit
import Combine

final class StartUpView: UIView {
    private lazy var startButton: UIButton = {
        return .makeFilled(title: "Start",
                           accessibilityLabel: "Start button")
    }()
    
    private lazy var titleLabel: UILabel = {
        return .makeLargeTitle(text: "Welcome to the Fibonacci calculator",
                               accessibilityLabel: "Main label")
    }()
    
    private lazy var subtitleLabel: UILabel = {
        return .makeSubtitle(text: "This app is a Fibonnaci number calculator.\nRemember that calculating big numbers might take time.",
                             accessibilityLabel: "subtitle label")
    }()
    
    private let viewModel: StartUpViewModelType
    
    init(viewModel: StartUpViewModelType) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func onStartTap() {
        viewModel.startTap()
    }
    
    private func setupView() {
        backgroundColor = .systemBackground
        addSubview(startButton)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        startButton.addTarget(self, action: #selector(onStartTap), for: .touchUpInside)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            startButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -.xLarge),
            startButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .large),
            startButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.large)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .large),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.large),
            titleLabel.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor, constant: -.small)
        ])
        
        NSLayoutConstraint.activate([
            subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: startButton.topAnchor, constant: -.xxLarge)
        ])
    }
}
