//
//  StartUpViewModel.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import Foundation

protocol StartUpViewModelType: AnyObject {
    func startTap()
}

final class StartUpViewModel<C>: StartUpViewModelType where C: StartUpFlowCoordinator {
    private weak var coordinator: C?
    
    init(coordinator: C) {
        self.coordinator = coordinator
    }
    
    func startTap() {
        self.coordinator?.onContinueTap()
    }
}
