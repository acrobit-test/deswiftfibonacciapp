//
//  StartUpViewController.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit
import Combine

protocol StartUpFlowCoordinator: AnyObject {
    func onContinueTap()
}

final class StartUpViewController: UIViewController {
    private let rootView: StartUpView
    
    init(rootView: StartUpView) {
        self.rootView = rootView
        super.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        view = rootView
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
