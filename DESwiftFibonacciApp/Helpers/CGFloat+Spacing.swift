//
//  CGFloat+Spacing.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit

extension CGFloat {
    /// value: 4
    public static let tiny: CGFloat = 4
    /// value: 8
    public static let small: CGFloat = 8
    /// value: 12
    public static let xSmall: CGFloat = 12
    /// value: 16
    public static let medium: CGFloat = 16
    /// value: 24
    public static let xMedium: CGFloat = 20
    /// value: 32
    public static let large: CGFloat = 24
    /// value: 48
    public static let xLarge: CGFloat = 32
    /// value: 64
    public static let xxLarge: CGFloat = 48
}
