//
//  +NumberFormatter.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 2/02/23.
//

import Foundation

extension NumberFormatter {
    static var fibonacciFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        return formatter
    }()
}
