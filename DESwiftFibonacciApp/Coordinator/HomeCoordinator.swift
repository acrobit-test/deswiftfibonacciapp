//
//  HomeCoordinator.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit

final class HomeCoordinator: NavigationCoordinator {
    override func start() {
        let viewModel = StartUpViewModel(coordinator: self)
        let view = StartUpView(viewModel: viewModel)
        let viewController = StartUpViewController(rootView: view)
        navigationController?.pushViewController(viewController, animated: false)
    }
}

extension HomeCoordinator: StartUpFlowCoordinator {
    func onContinueTap() {
        let viewModel = CalculationViewModel(coordinator: self)
        let view = CalculationView(viewModel: viewModel)
        let viewController = CalculationViewController(rootView: view, viewModel: viewModel)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension HomeCoordinator: CalculationFlowCoordinator {
    
}
