//
//  NavigationCoordinator.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import UIKit

open class NavigationCoordinator: UIViewController, Coordinator {
    private let _navigationController = UINavigationController()
    
    open override var navigationController: UINavigationController? {
        _navigationController
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        let navigationView = _navigationController.view!
        self.view.addSubview(navigationView)
        navigationView.translatesAutoresizingMaskIntoConstraints = false
        addChild(_navigationController)
        NSLayoutConstraint.activate([
            navigationView.topAnchor.constraint(equalTo: view.topAnchor),
            navigationView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            navigationView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            navigationView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        _navigationController.didMove(toParent: self)
    }
    
    func start() {
        fatalError("Needs to be implemented")
    }
}
