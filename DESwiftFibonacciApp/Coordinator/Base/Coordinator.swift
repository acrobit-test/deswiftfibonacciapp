//
//  Coordinator.swift
//  DESwiftFibonacciApp
//
//  Created by Diego Espinoza on 31/01/23.
//

import Foundation

protocol Coordinator: AnyObject {
    func start()
}
